import 'package:app_tacos/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        AppLocalizationDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: AppLocalizations.delegate.supportedLocales,
      title: 'Material App',
      home: MyTacos(),
    );
  }
}

class MyTacos extends StatefulWidget {
  MyTacos({Key? key}) : super(key: key);

  @override
  State<MyTacos> createState() => _MyTacosState();
}

class _MyTacosState extends State<MyTacos> {
  var a = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            flex: 4,
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: [
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                          "https://images.unsplash.com/photo-1545093149-618ce3bcf49d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"),
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 4,
            child: Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(horizontal: 10),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppLocalizations.of(context).titulo,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 1.3,
                    ),
                  ),

                  // ! Nombre del taco
                  Text(
                    AppLocalizations.of(context).subtitulo,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 1.3,
                    ),
                  ),

                  SizedBox(
                    height: 20,
                  ),

                  // ! Precio del taco
                  Text(
                    "\$20.00",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 18,
                      letterSpacing: 1.3,
                    ),
                  ),

                  SizedBox(
                    height: 20,
                  ),

                  // ! Descripción del taco
                  Text(
                    AppLocalizations.of(context).descripcion,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontSize: 12,
                        letterSpacing: 0.4,
                        wordSpacing: 1.1,
                        fontWeight: FontWeight.w500,
                        color: Colors.black54,
                        height: 1.5),
                  ),

                  // ! Boton
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 12, vertical: 12),
                          decoration: BoxDecoration(
                              color: Colors.blueGrey.shade200,
                              borderRadius: BorderRadius.circular(50)),
                          child: Text(
                            AppLocalizations.of(context).titulo_boton,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w800,
                                fontSize: 20),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 70,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    a = 1;
                  });
                },
                child: Icon(
                  Icons.category_outlined, 
                  color: a != 1 ? Colors.grey : Colors.red.shade700
                  ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    a = 2;
                  });
                },
                child: Icon(
                  Icons.category_outlined, 
                  color: a != 2 ? Colors.grey : Colors.red.shade700
                  ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    a = 3;
                  });
                },
                child: Icon(
                  Icons.category_outlined, 
                  color: a != 3 ? Colors.grey : Colors.red.shade700
                  ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
